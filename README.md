# Web Crawler Sitemap

[![Build Status](https://travis-ci.org/florx/web-crawler-sitemap.svg?branch=master)](https://travis-ci.org/florx/web-crawler-sitemap)

This is a simple web crawler written in Java. It limits crawling domain of the initial domain you give it, but will produce a sitemap containing all the external links, stylesheets, images and Javascript files also.

It was written in TDD format, and they can all be run using ```mvn clean install``` on the project root.

Then you can execute the application using:

```java -jar target/webcrawler-0.0.1-SNAPSHOT-jar-with-dependencies.jar http://buildit.florxlabs.com/5```

## Assumptions/Future Improvements

* Links that have #anchors at the end are the same as without the anchor (so I strip the anchor off)
* To improve performance in the future, I should make it threaded.
* Taking above further, I could setup a distributed network where lots of different nodes request links from one control server, and report back a list of links. This would spread the load.
* There's no Browser Useragent set.
* We don't currently follow robots.txt - see <http://www.robotstxt.org/>
* There's no parsing of the URLs to see if they're likely to be actual HTML files. So we visit CSS ones too.
* There's no verification that assets actually exist, like with a href/css. If we get 404 or similar, we throw away the link.