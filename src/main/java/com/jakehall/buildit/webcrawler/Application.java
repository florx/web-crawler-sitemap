package com.jakehall.buildit.webcrawler;

import java.util.Set;

public class Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		if (args.length == 0) {
			System.out.println("Proper Usage is: java Application startUrl");
			System.exit(0);
		}

		Crawler c = new Crawler(args[0]);
		Set<String> sitemap = c.getSitemap();
		
		System.out.println("--------------------------");
		System.out.println("Run complete. Sitemap is:");

		for (String link : sitemap) {
			System.out.println(link);
		}

	}

}
