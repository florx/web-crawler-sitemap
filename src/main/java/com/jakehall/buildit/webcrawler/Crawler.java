package com.jakehall.buildit.webcrawler;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Crawler {

	private boolean debug;
	private int visitedCount = 0;
	private String domain;
	private Set<String> visitedLinks = new HashSet<String>();
	private Set<String> sitemap = new HashSet<String>();
	private Set<String> blacklistLinks = new HashSet<String>();
	private List<String> pendingLinks = new ArrayList<String>();

	public Crawler(String startUrl) {
		this(startUrl, false);
	}
	
	public Crawler(String startUrl, boolean debug) {
		if(debug) System.out.println("------------------------------");
		if(debug) System.out.println("DEBUG MODE");
		
		this.debug = debug;
		if(debug) System.out.println("Starting with: " + startUrl);
		pendingLinks.add(startUrl);
		

		try {
			domain = getDomainName(startUrl);
			this.run();
		} catch (URISyntaxException e) {
			System.out.println("A problem occured:");
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Recursively called to crawl the pendingLinks available.
	 */
	private void run() {

		crawl(pendingLinks.remove(0));

		if (pendingLinks.size() > 0) {
			this.run();
		}

	}

	/**
	 * Main crawler
	 * 
	 * @param url
	 */
	private void crawl(String url) {

		if (!checkBlacklist(url))
			return;

		if (!checkIfInDomain(url))
			return;

		if (!checkUniqueUrl(url))
			return;

		if(debug) System.out.println("Requesting " + url);

		try {

			Document webPage = Jsoup.connect(url).get();
			visitedCount++;
			visitedLinks.add(url);
			sitemap.add(url);
			Elements linksOnPage = webPage.select("[href]");

			for (Element link : linksOnPage) {
				this.pendingLinks.add(removeURLAnchor(link.absUrl("href")));
			}

			Elements srcOnPage = webPage.select("[src]");
			for (Element link : srcOnPage) {
				this.sitemap.add(link.absUrl("src"));
			}

		} catch (IOException e) {
			blacklistLinks.add(url);
			if(debug) System.out.println("Could not retreive " + url + ": " + e.getMessage());
		} catch (IllegalArgumentException e){
			blacklistLinks.add(url);
			System.out.println("Could not retreive " + url + ": " + e.getMessage());
		}

	}

	/**
	 * This tests if the URL we are trying to crawl is in the same domain as the
	 * one we first specified. If it isn't we just skip it.
	 * 
	 * @param url
	 * @return boolean
	 */
	private boolean checkIfInDomain(String url) {

		try {
			if (!this.domain.equals(getDomainName(url))) {
				if(debug) System.out.println("Skipping (outside domain) " + url);
				sitemap.add(url);
				return false;
			}
		} catch (URISyntaxException e) {
			// not really interested if it's not a valid URL, just skip it
			// anyway
			if(debug) System.out.println("Skipping (invalid URL) " + url);
			return false;
		}

		return true;
	}

	/**
	 * Check to see if we have already visited the URL before, if we have we can
	 * safely skip it!
	 * 
	 * @param url
	 * @return boolean
	 */
	private boolean checkUniqueUrl(String url) {

		if (visitedLinks.contains(url)) {
			if(debug) System.out.println("Skipping (already seen) " + url);
			return false;
		}

		return true;
	}

	/**
	 * Check to see if the URL is blacklisted (if we've already tried to visit
	 * and it failed due to 404 or similar)
	 * 
	 * @param url
	 * @return boolean
	 */
	private boolean checkBlacklist(String url) {
		
		if (blacklistLinks.contains(url)) {
			if(debug) System.out.println("Skipping (blacklist) " + url);
			return false;
		}

		return true;
	}

	public Set<String> getVisited() {
		return visitedLinks;
	}
	
	public Set<String> getSitemap() {
		return sitemap;
	}

	/**
	 * Used to test that page loops have been fixed.
	 * 
	 * @return visitedCount
	 */
	public int getVisitedCount() {
		return visitedCount;
	}

	/**
	 * Strip all but the domain (and also www.) from the URL)
	 * 
	 * @param url
	 * @return String domainName
	 * @throws URISyntaxException
	 */
	private String getDomainName(String url) throws URISyntaxException {
		URI uri = new URI(url);
		String domain = uri.getHost();
		if (domain == null)
			return "";
		return domain.startsWith("www.") ? domain.substring(4) : domain;
	}

	/**
	 * Some links may have an anchor on the end (e.g.
	 * http://google.com/link.html#anchor) we don't need/want the #anchor bit
	 * 
	 * @param url
	 * @return String cleaned URL
	 */
	private String removeURLAnchor(String url) {
		String[] urlArray = url.split("#");
		return urlArray[0];
	}

}
