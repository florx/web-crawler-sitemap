package com.jakehall.buildit.webcrawler.test;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import com.jakehall.buildit.webcrawler.Crawler;

public class CrawlerTest {

	@Test
	public void testSimpleTwoLinks() {
		Crawler c = new Crawler("http://buildit.florxlabs.com/1/");

		Set<String> eqS = Stream.of("http://buildit.florxlabs.com/1/", "http://buildit.florxlabs.com/1/a.html")
				.collect(Collectors.toCollection(HashSet::new));
		assertEquals(eqS, c.getSitemap());
		assertEquals(2, c.getVisitedCount());
	}

	@Test
	public void testSimpleStayWithinDomain() {
		Crawler c = new Crawler("http://buildit.florxlabs.com/2/");

		Set<String> eqS = Stream.of("http://buildit.florxlabs.com/2/", "http://buildit.florxlabs.com/2/a.html",
				"http://facebook.com", "http://www.google.com").collect(Collectors.toCollection(HashSet::new));
		assertEquals(eqS, c.getSitemap());
		assertEquals(2, c.getVisitedCount());

		Set<String> eqV = Stream.of("http://buildit.florxlabs.com/2/", "http://buildit.florxlabs.com/2/a.html")
				.collect(Collectors.toCollection(HashSet::new));
		assertEquals(eqV, c.getVisited());
	}

	@Test
	public void testSimpleGetImages() {
		Crawler c = new Crawler("http://buildit.florxlabs.com/3/");

		Set<String> eqS = Stream.of("http://buildit.florxlabs.com/3/", "http://buildit.florxlabs.com/3/a.html",
				"http://buildit.florxlabs.com/img/canadian-flag-medium.jpg",
				"http://buildit.florxlabs.com/img/cpaneltrash.png", "http://buildit.florxlabs.com/img/deletekey.png",
				"http://buildit.florxlabs.com/img/heidiscreenshot.png").collect(Collectors.toCollection(HashSet::new));
		assertEquals(eqS, c.getSitemap());
		assertEquals(2, c.getVisitedCount());
	}

	@Test
	public void testSimpleGetStylesheets() {
		Crawler c = new Crawler("http://buildit.florxlabs.com/4/");

		Set<String> eqS = Stream.of("http://buildit.florxlabs.com/4/", "http://buildit.florxlabs.com/4/a.html",
				"http://buildit.florxlabs.com/style/reset.css", "http://buildit.florxlabs.com/style/typography.css",
				"http://buildit.florxlabs.com/style/style.css", "http://blog.florxlabs.com", "http://www.jakehall.io",
				"http://twitter.com/florx", "http://bitbucket.org/florx", "http://facebook.com/hall.jake.a",
				"https://uk.linkedin.com/in/halljakea").collect(Collectors.toCollection(HashSet::new));
		assertEquals(eqS, c.getSitemap());
		assertEquals(5, c.getVisitedCount());

		Set<String> eqV = Stream.of("http://buildit.florxlabs.com/4/", "http://buildit.florxlabs.com/4/a.html",
				"http://buildit.florxlabs.com/style/reset.css", "http://buildit.florxlabs.com/style/typography.css",
				"http://buildit.florxlabs.com/style/style.css").collect(Collectors.toCollection(HashSet::new));
		assertEquals(eqV, c.getVisited());
	}

	@Test
	public void testComplexImgScriptStylesheet() {
		Crawler c = new Crawler("http://buildit.florxlabs.com/5/index.html");

		Set<String> eqS = Stream.of("http://buildit.florxlabs.com/5/index.html",
				"http://buildit.florxlabs.com/5/a.html", "http://buildit.florxlabs.com/style/reset.css",
				"http://buildit.florxlabs.com/style/typography.css", "http://buildit.florxlabs.com/style/style.css",
				"http://buildit.florxlabs.com/js/script.js", "http://blog.florxlabs.com", "http://www.jakehall.io",
				"http://twitter.com/florx", "http://bitbucket.org/florx", "http://facebook.com/hall.jake.a",
				"https://uk.linkedin.com/in/halljakea", "http://buildit.florxlabs.com/img/canadian-flag-medium.jpg",
				"http://buildit.florxlabs.com/img/cpaneltrash.png", "http://buildit.florxlabs.com/img/deletekey.png",
				"http://buildit.florxlabs.com/img/heidiscreenshot.png").collect(Collectors.toCollection(HashSet::new));
		assertEquals(eqS, c.getSitemap());
		assertEquals(5, c.getVisitedCount());

		Set<String> eqV = Stream.of("http://buildit.florxlabs.com/5/index.html",
				"http://buildit.florxlabs.com/5/a.html", "http://buildit.florxlabs.com/style/reset.css",
				"http://buildit.florxlabs.com/style/typography.css", "http://buildit.florxlabs.com/style/style.css")
				.collect(Collectors.toCollection(HashSet::new));
		assertEquals(eqV, c.getVisited());
	}

	@Test
	public void testSimpleMissingLinks() {
		Crawler c = new Crawler("http://buildit.florxlabs.com/6/");

		Set<String> eqS = Stream.of("http://buildit.florxlabs.com/6/", "http://buildit.florxlabs.com/6/a.html")
				.collect(Collectors.toCollection(HashSet::new));
		assertEquals(eqS, c.getSitemap());
		assertEquals(2, c.getVisitedCount());
	}
}
